# Troubleshooting errors
---

### 401 Unauthorized
- Ensure that you have provided an API Key using a query parameter named key, which should look like the following:
    - "https://host/login?key={APIKEY}"
- Ensure that you have provided the JWT Access Token in the **Authorization Header** using the **Bearer** schema. The content of the header should look like the following:
    - Authorization: Bearer {token}