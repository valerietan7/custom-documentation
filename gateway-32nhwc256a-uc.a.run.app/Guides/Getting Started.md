# Getting Started
---

### Before you begin
1. You will need to be registered with [Auk Industries](https://auk.industries/ "Auk Industries") to use these APIs.
2. You would also have access to an API Key.


### Getting the JSON Web Token (JWT) 
1. You will need a JWT, which can be found by logging into your account with your credentials using the Login API.
2. Supply the API Key that you have using a query parameter named key, shown in the following example:
- "https://host/login?key={APIKEY}"
3. After logging in, the response will contain an object that has the **Access Token**.
4. Copy the Access Token to the clipboard.

### Using the API
1. Browse the reference section of this site to see examples of what you can do with this API and how to use it. 
2. For every request:
- Supply the API Key that you have using a query parameter named key, similar to using the login API above
- Send the Access Token in the **Authorization Header** using the **Bearer** schema. The content of the header should look like the following:
    - Authorization: Bearer {token}
